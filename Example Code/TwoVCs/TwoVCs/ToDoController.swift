//
//  ToDoController.swift
//  TwoVCs
//
//  Created by michael.ziray on 9/20/17.
//  Copyright © 2017 Boise State. All rights reserved.
//

import UIKit

class ToDoController: NSObject {

    static var todosArray:Array<String> = []
    
    class func addToDo(newToDo:String) {
        
        ToDoController.todosArray.append(newToDo)
        
    }
    
    class func removeToDo(indexToRemove:Int){
        ToDoController.todosArray.remove(at: indexToRemove)
    }
}
