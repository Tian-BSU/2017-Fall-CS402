//
//  ViewController.swift
//  Maps
//
//  Created by michael.ziray on 9/12/17.
//  Copyright © 2017 Boise State. All rights reserved.
//

import UIKit
import MapKit


class ViewController: UIViewController {

    @IBOutlet var mapView:MKMapView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func centerWasTapped(_ sender: Any) {
//        43.61528502754942
//        -116.2034010887146
//        let mapAnnotation:MKAnnotation = MKAnnotation() // Abstract, can't call directly
        let mapAnnotation:MKPointAnnotation = MKPointAnnotation()
        mapAnnotation.coordinate = CLLocationCoordinate2DMake(43.61528502754942, -116.2034010887146)
        mapAnnotation.title = "Mobile Dev"
        mapAnnotation.subtitle = "CS402"
        mapView.addAnnotation(mapAnnotation)
        
        let coordinateSpan:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let coordinateRegion:MKCoordinateRegion = MKCoordinateRegion(center:  mapAnnotation.coordinate, span: coordinateSpan)
        mapView.setRegion(coordinateRegion, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

